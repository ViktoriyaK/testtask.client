﻿namespace MobilePhones.Wpf.Models
{
    /// <summary>
    /// Represents a message type.
    /// </summary>
    public enum MessageType { Added, Deleted, Changed }

    /// <summary>
    /// Represents a mobile phone message.
    /// </summary>
    public class MobilePhonesMessage
    {
        /// <summary>
        /// Gets or sets a message type.
        /// </summary>
        public MessageType MessageType { get; private set; }

        /// <summary>
        /// Gets or sets a mobile phone.
        /// </summary>
        public MobilePhone MobilePhone { get; private set; }

        /// <summary>
        /// Initialize a new <see cref="MobilePhonesMessage"/>.
        /// </summary>
        /// <param name="mobilePhone">A <see cref="MobilePhone"/>.</param>
        /// <param name="messageType">A <see cref="MessageType"/>.</param>
        public MobilePhonesMessage(MobilePhone mobilePhone, MessageType messageType)
        {
            MobilePhone = mobilePhone;
            MessageType = messageType;
        }
    }
}
