﻿using MobilePhones.Wpf.Models;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MobilePhones.Wpf.Services
{
    /// <summary>
    /// Represents a service for working with mobile phones web api server by HttpClient.
    /// </summary>
    public class MobilePhonesWebApiClient
    {
        private readonly HttpClient _httpClient;

        /// <summary>
        /// Initialize a new instanse of <see cref="MobilePhonesWebApiClient"/>.
        /// </summary>
        public MobilePhonesWebApiClient()
        {
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://localhost:44313/index.html")
            };
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        }

        /// <summary>
        /// Sends GET request for getting all mobile phones.
        /// </summary>
        /// <returns>A <see cref="Task{ObservableCollection{MobilePhone}}"/></returns>
        public async Task<ObservableCollection<MobilePhone>> GetMobilePhonesAsync()
        {
            HttpResponseMessage response = await _httpClient.GetAsync("/api/phones");
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsAsync<ObservableCollection<MobilePhone>>();
        }

        /// <summary>
        /// Sends PUT request for updating a mobile phone.
        /// </summary>
        /// <param name="id">A mobile phone identifier.</param>
        /// <param name="name">A new mobile phone name.</param>
        /// <param name="photoPath">A new photo path of a mobile phone.</param>
        /// <returns>A <see cref="Task"/></returns>
        public async Task UpdateMobilePhoneAsync(int id, string name, string photoPath = null)
        {
            var requestContent = new MultipartFormDataContent();

            if (photoPath != null)
            {
                var imageContent = new ByteArrayContent(File.ReadAllBytes(photoPath));
                imageContent.Headers.ContentType =
                    MediaTypeHeaderValue.Parse("image/jpeg");

                requestContent.Add(imageContent, "PhotoFile", Path.GetFileName(photoPath));
            }

            requestContent.Add(new StringContent(name, Encoding.UTF8, "application/json"), "Name");

            var response = await _httpClient.PutAsync($"api/phones/{id}", requestContent);

            response.EnsureSuccessStatusCode();
        }

        /// <summary>
        /// Sends POST request for creating new mobile phone.
        /// </summary>
        /// <param name="name">A name of a new mobile phone.</param>
        /// <param name="photoPath">A photo path of a new mobile phone.</param>
        /// <returns>A <see cref="Task{MobilePhone}"/></returns>
        public async Task<MobilePhone> CreateMobilePhoneAsync(string name, string photoPath)
        {
            var requestContent = new MultipartFormDataContent();

            var imageContent = new ByteArrayContent(File.ReadAllBytes(photoPath));
            imageContent.Headers.ContentType =
                MediaTypeHeaderValue.Parse("image/jpeg");

            requestContent.Add(new StringContent(name, Encoding.UTF8, "application/json"), "Name");
            requestContent.Add(imageContent, "PhotoFile", Path.GetFileName(photoPath));

            var response = await _httpClient.PostAsync("api/phones/", requestContent);

            response.EnsureSuccessStatusCode();

            return await GetMobilePhoneAsync(response.Headers.Location.ToString());
        }

        /// <summary>
        /// Sends DELETE request for deleting a mobile phone.
        /// </summary>
        /// <param name="id">A mobile phone identifier.</param>
        /// <returns>A <see cref="Task"/></returns>
        public async Task DeleteMobilePhoneAsync(int id)
        {
            var response = await _httpClient.DeleteAsync($"api/phones/{id}");
            response.EnsureSuccessStatusCode();
        }

        /// <summary>
        /// Returns a url for getting mobile phone photo image.
        /// </summary>
        /// <param name="id">A mobile phone identifier.</param>
        /// <returns>A url of a mobile phone photo image.</returns>
        public string GetPhotoUrl(int id)
            => $"https://localhost:44313/api/phones/{id}/image";

        private async Task<MobilePhone> GetMobilePhoneAsync(string path)
        {
            MobilePhone mobilePhone = null;
            HttpResponseMessage response = await _httpClient.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                mobilePhone = await response.Content.ReadAsAsync<MobilePhone>();
            }
            return mobilePhone;
        }
    }
}
