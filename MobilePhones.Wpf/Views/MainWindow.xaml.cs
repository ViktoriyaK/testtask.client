﻿using MobilePhones.Wpf.ViewModels;
using System.Windows;

namespace MobilePhones.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            SetMainContext();
        }

        private async void SetMainContext()
        {
            var mainViewModel = await MainViewModel.BuildMainViewModelAsync();
            PhonesListView.ItemsSource = mainViewModel?.MobilePhones;
            MainContainer.DataContext = mainViewModel;
        }
    }
}
