﻿using DevExpress.Mvvm;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using MobilePhones.Wpf.Models;
using MobilePhones.Wpf.Services;
using MobilePhones.Wpf.Views;
using System;
using System.Windows;
using System.Windows.Input;

namespace MobilePhones.Wpf.ViewModels
{
    public class CreateMobilePhoneViewModel : BindableBase
    {
        public string Name
        {
            get { return GetProperty(() => Name); }
            set { SetProperty(() => Name, value); }
        }

        private string uploadedImagePath;
        public string PhotoSource
        {
            get
            {
                if (uploadedImagePath != null)
                    return uploadedImagePath;

                return "/MobilePhones.Wpf;component/Images/images.png";
            }
            set
            {
                uploadedImagePath = value;
                RaisePropertyChanged(() => PhotoSource);
            }
        }

        public bool IsPhotoRequired
        {
            get { return GetProperty(() => IsPhotoRequired); } 
            set { SetProperty(() => IsPhotoRequired, value); }
        }

        public CreateMobilePhoneViewModel()
        {
            IsPhotoRequired = true;
        }

        public ICommand PickPhoto
            => new DelegateCommand(() =>
            {
                OpenFileDialog fileDialog = new OpenFileDialog
                {
                    DefaultExt = ".jpg",
                    Filter = "Image files (.jpg)|*.jpg"
                };

                if (fileDialog.ShowDialog() == true)
                {
                    uploadedImagePath = fileDialog.FileName;
                    IsPhotoRequired = false;
                    RaisePropertyChanged(() => PhotoSource);
                }
                else
                {
                    uploadedImagePath = null;
                    IsPhotoRequired = true;
                    RaisePropertyChanged(() => PhotoSource);
                }
            });

        public ICommand Save
            => new DelegateCommand(async () =>
            {
                if (uploadedImagePath == null || Name == null)
                {
                    MessageBox.Show("Fill all required fields please.");
                    return;
                }
                try
                {
                    var createdMobilePhone = await (new MobilePhonesWebApiClient()).CreateMobilePhoneAsync(Name, uploadedImagePath);
                        Messenger.Default.Send(new MobilePhonesMessage(createdMobilePhone, MessageType.Added));
                        DialogHost.CloseDialogCommand.Execute(null, null);
                }
                catch (System.Net.Http.HttpRequestException e)
                {
                    MessageBox.Show("Something wrong with request to server.");
                }
            });
    }
}
