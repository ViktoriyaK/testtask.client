﻿using DevExpress.Mvvm;
using MaterialDesignThemes.Wpf;
using Microsoft.Win32;
using MobilePhones.Wpf.Models;
using MobilePhones.Wpf.Services;
using MobilePhones.Wpf.Views;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace MobilePhones.Wpf.ViewModels
{
    public class MobilePhoneViewModel : BindableBase
    {
        private readonly MobilePhonesWebApiClient webService = new MobilePhonesWebApiClient();

        public int Id { get; set; }

        public string Name
        {
            get { return GetProperty(() => Name); }
            set { SetProperty(() => Name, value); }
        }

        public BitmapImage PhotoSource
        {
            get { return GetProperty(() => PhotoSource); }
            set { SetProperty(() => PhotoSource, value); }
        }

        private string updatedName;
        public string UpdatedName
        {
            get
            {
                if (updatedName != null)
                    return updatedName;

                return Name;
            }
            set
            {
                updatedName = value;
                RaisePropertyChanged(() => UpdatedName);
            }
        }

        private BitmapImage updatedPhoto;
        public BitmapImage UpdatedPhotoSource
        {
            get
            {
                if (updatedPhoto != null)
                    return updatedPhoto;

                return new BitmapImage(new Uri(webService.GetPhotoUrl(Id)));
            }
            set
            {
                updatedPhoto = value;
                RaisePropertyChanged(() => UpdatedPhotoSource);
            }
        }

        private string newPhotoPath;

        public MobilePhoneViewModel(MobilePhone mobilePhone)
        {
            Id = mobilePhone.Id;
            Name = mobilePhone.Name;
            PhotoSource = new BitmapImage(new Uri(webService.GetPhotoUrl(Id)));
        }

        public ICommand ChangePhoto
            => new DelegateCommand(() =>
            {
                OpenFileDialog fileDialog = new OpenFileDialog
                {
                    DefaultExt = ".jpg",
                    Filter = "Image files (.jpg)|*.jpg"
                };

                if (fileDialog.ShowDialog() == true)
                {
                    UpdatedPhotoSource = new BitmapImage(new Uri(fileDialog.FileName));
                    newPhotoPath = fileDialog.FileName;
                }
            });

        public ICommand Save
            => new DelegateCommand(async () =>
            {
                if (Name != UpdatedName || updatedPhoto != null)
                {
                    Name = UpdatedName;
                    try
                    {
                        await webService.UpdateMobilePhoneAsync(Id, Name, newPhotoPath);
                        if (updatedPhoto != null)
                            PhotoSource = updatedPhoto;
                    }
                    catch(System.Net.Http.HttpRequestException e)
                    {
                        MessageBox.Show("Something wrong with request to server.");
                    }                    
                }
            });

        public ICommand DeleteMobilePhone 
            => new DelegateCommand(async () =>
            {
                var view = new DeleteConfirmationDialog();

                //show the dialog
                var result = (bool)await DialogHost.Show(view, "RootDialog");

                if (result == true)
                {
                    try
                    {
                        await webService.DeleteMobilePhoneAsync(Id);
                        Messenger.Default.Send(new MobilePhonesMessage(new MobilePhone { Id = this.Id }, MessageType.Deleted));
                    }
                    catch (System.Net.Http.HttpRequestException e)
                    {
                        MessageBox.Show("Something wrong with request to server.");
                    }
                }
            });
    }
}
