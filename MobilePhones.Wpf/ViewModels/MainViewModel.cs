﻿using DevExpress.Mvvm;
using MaterialDesignThemes.Wpf;
using MobilePhones.Wpf.Models;
using MobilePhones.Wpf.Services;
using MobilePhones.Wpf.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace MobilePhones.Wpf.ViewModels
{
    public class MainViewModel
    {
        public ObservableCollection<MobilePhoneViewModel> MobilePhones { get; set; }

        public static async Task<MainViewModel> BuildMainViewModelAsync()
        {
            try
            {
                var mobilePhones = await new MobilePhonesWebApiClient().GetMobilePhonesAsync();
                return new MainViewModel(mobilePhones);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return null;
            }
        }

        private MainViewModel(ObservableCollection<MobilePhone> mobilePhones)
        {
            this.MobilePhones = new ObservableCollection<MobilePhoneViewModel>(mobilePhones.Select(ph => new MobilePhoneViewModel(ph)));
            Messenger.Default.Register<MobilePhonesMessage>(this, OnMessage);
        }

        public ICommand CreateMobilePhone
            => new DelegateCommand(async () =>
            {
                var view = new CreateMobilePhoneDialog
                {
                    DataContext = new CreateMobilePhoneViewModel()
                };

                await DialogHost.Show(view, "RootDialog");
            });

        void OnMessage(MobilePhonesMessage message)
        {
            switch (message.MessageType)
            {
                case MessageType.Added:
                    this.MobilePhones.Add(new MobilePhoneViewModel(message.MobilePhone));
                    break;
                case MessageType.Changed:
                    //... 
                    break;
                case MessageType.Deleted:
                    var mobilePhone = MobilePhones.FirstOrDefault(m => m.Id == message.MobilePhone.Id);
                    if (mobilePhone != null)
                        MobilePhones.Remove(mobilePhone);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
